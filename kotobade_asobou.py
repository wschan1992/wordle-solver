#!/usr/bin/env python3

import os, sys, argparse
from modules.JPWordleSolver import KotobadeAsobouSolver

#===============================================================================
if __name__ == "__main__":
    parser = argparse.ArgumentParser('kotobade_asobou.py')

    parser.add_argument("--legacy", "-L", action="store_true",
                        help="Legacy mode. Do not distinguish kogaki from " \
                        "normal kana. No hint mode.")
    parser.add_argument("--mode", "-m", default="normal",
                        choices=['normal', 'hard', 'strict'],
                        help="Game mode ('normal', 'hard' or 'strict'). " \
                             "'strict' mode: " \
                             "Only allowed to guess possible answers according " \
                             "to all known information.")
    parser.add_argument("--no-hint", "-H", action="store_true",
                        help="Turn off hint mode.")
    parser.add_argument("--max-suggests", "-n", type=int, default=25,
                        help="Maxinum number of suggestions")
    parser.add_argument("--max-guesses", "-g", type=int, default=12,
                        help="Maxinum number of guesses (0 = no limit).")
    parser.add_argument("--lexicon", "-l", type=str,
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                             "data", "jp_words_4char.tsv"),
                        help="Path to the list of possible words in tsv format. " \
                             "The tsv must have two columns: "\
                             "the first contains the words in hiraganas; " \
                             "the second contains the kanji forms or other annotations.")

#===============================================================================
def kotobade_asobou(args):
    solver = KotobadeAsobouSolver(mode=args.mode,
                                    legacy=args.legacy,
                                    no_hint=args.no_hint,
                                    max_guesses=args.max_guesses,
                                    max_suggests=args.max_suggests)
    solver.initialize(args.lexicon)
    solver.startNewGame()

#===============================================================================
if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])
    exit(kotobade_asobou(args))
