import os, sys
import regex as re
import json
import hashlib
from collections import defaultdict
import numpy as np
from .Wordle import Wordle

class WordleSolver:
    def __init__(self, mode="normal", max_guesses=0, max_suggests=10):
        self.mode = mode
        self.max_guesses = max_guesses
        self.num_guesses = 0
        self.max_suggests = max_suggests

        self.lexicon_hash = None
        self.all_words = None
        self.all_games = None
        self.starting_entropies = None

        self.possible_games = None
        self.allowed_guesses = None
        self.entropies = None

    def initialize(self, lexicon=None):
        if self.mode=="normal":
            print("Running in normal mode.")
        elif self.mode=="hard":
            print("Running in hard mode.")
        elif self.mode=="strict":
            print("Running in strict mode " \
                  "(all previous hints must be utlized).")
        else:
            raise RuntimeError("Unknown mode '{}' " \
                  "(should be 'normal', 'hard', or 'strict')".format(self.mode))

        if self.starting_entropies is None:
            if lexicon is None:
                raise RuntimeError("Need path to a lexicon to initialize.")

            print("Loading word list...")

            self.readLexicon(lexicon)

            print("Found {:d} words...".format(len(self.all_words)))

            self.lexicon_hash = None
            md5 = hashlib.md5()
            with open(lexicon, "rb") as f:
                md5.update(f.read())
            self.lexicon_hash = md5.hexdigest()

            self.all_games = {}
            for word in self.all_words:
                self.all_games[word] = self.Game(word)
            self.possible_games = dict(self.all_games)

            # It takes a while to compute all possible opening guesses, and the results
            # are always the same every time - so we do not recompute if the lexicon did
            # not change
            dirname, basename = os.path.split(lexicon)
            cache_file = os.path.join(dirname, "." + basename + ".entropy.json")

            entropies = self.loadCachedEntropies(cache_file)
            if entropies is None:
                entropies = self.calculateEntropies(self.all_words)
                self.saveCachedEntropies(entropies, cache_file)
            self.starting_entropies = self.entropies = entropies

        self.possible_games = dict(self.all_games)
        self.allowed_guesses = list(self.all_words.keys())
        self.num_guesses = 0

        print("Ready!")

    def startNewGame(self, lexicon=None):
        if lexicon is not None:
            self.initialize(lexicon)
        elif self.starting_entropies is None:
            raise RuntimeError("WordleSolver not initialized.")
        else:
            self.possible_games = dict(self.all_games)
            self.allowed_guesses = list(self.all_words.keys())
            self.num_guesses = 0

        self.showBestOpens(self.max_suggests)

        game_continues = True
        while game_continues:
            if self.max_guesses == 0:
                msg = "Enter your guess"
            else:
                msg = "Enter your {:d}/{:d} guess".format(self.num_guesses+1,
                                                          self.max_guesses)
            instruction = input("{} (or " \
                                "show possible [a]nswers / " \
                                "show possible [g]uesses / " \
                                "[r]estart / [q]uit): ".format(msg))

            if len(instruction) == 0:
                continue
            elif instruction.lower() in ("a", "answers"):
                show_all = True
                if len(self.possible_games) > 100:
                    confirm = input("There are {:d} possible answers, show all? " \
                                    "(y/N): ".format(len(self.possible_games)))
                    show_all = "yes".startswith(confirm.lower()) and not confirm == ""
                if show_all:
                    self.showPossibleAnswers()
            elif instruction.lower() in ("g", "guesses"):
                show_all = True
                if len(self.allowed_guesses) > 100:
                    confirm = input("There are {:d} possible guesses, show all? " \
                                    "(y/N): ".format(len(self.allowed_guesses)))
                    show_all = "yes".startswith(confirm.lower()) and not confirm == ""
                if show_all:
                    self.showBestGuesses()
            elif instruction.lower() in ("r", "restart"):
                print("Restarting...")
                self.initialize()
                self.showBestOpens(self.max_suggests)
                continue
            elif instruction.lower() in ("q", "quit"):
                print("Bye! Have a good day!")
                exit(0)
            elif self.validGuess(instruction) is not None:
                game_continues = self.nextGuess(guess=self.validGuess(instruction),
                                                pattern=self.inputPattern())

    def nextGuess(self, guess, pattern):
        # Return False when the game is ended, else True
        if guess is None or pattern is None:
            return True

        self.num_guesses += 1
        if pattern == "2"*len(guess):
            print("Congratulations!")
            return False
        elif self.num_guesses == self.max_guesses:
            print("Sorry... Better luck next time!")
            return False

        # Update possible games
        updated_possible_games = dict(self.possible_games)
        for word,game in self.possible_games.items():
            if not game.guess(guess) == game.patternStringToList(pattern):
                del(updated_possible_games[word])
        self.possible_games = updated_possible_games
        if len(self.possible_games) == 1:
            word = next(iter(self.possible_games))
            print("Only one possible word left in the lexicon:")
            self.showEntropies(only_possible_answers=True)
            return True
        elif len(self.possible_games) == 0:
            print("Oops. There is no more possible word from the loaded list. " \
                  "Try using another list with the option --lexicon/-l.")
            return False
        else:
            print("{:d} possible words left...".format(len(self.possible_games)))

        # Allowed guesses are restricted in hard / strict modes
        if self.mode=="hard":
            self.allowed_guesses = Wordle.hardModeRestricted(guess, pattern, self.allowed_guesses)
        elif self.mode=="strict":
            self.allowed_guesses = self.possible_games.keys()
        if len(self.allowed_guesses) == 0:
            print("Oops. There is no more possible words from the loaded list. " \
                  "Try using another list with the option --lexicon/-l.")
            return False

        # Calculate entropies
        self.entropies = self.calculateEntropies(self.allowed_guesses)
        self.showBestGuesses(self.max_suggests)

        return True

    def showBestGuesses(self, n=0):
        print("Best {:d} guesses:".format(min(n, len(self.allowed_guesses))))
        self.showEntropies(n)

    def showBestOpens(self, n=0):
        print("Best {:d} opens:".format(min(n, len(self.all_words))))
        self.showEntropies(n, initial=True)

    def showPossibleAnswers(self, n=0):
        print("Possible answers:")
        self.showEntropies(n, only_possible_answers=True)

    def showEntropies(self, n=0, only_possible_answers=False, initial=False):
        entropies = self.starting_entropies if initial else self.entropies
        if n < 1:
            n = len(entropies)
        for word,entr in sorted(entropies.items(),
                                key=lambda x: (x[1], x[0] in self.possible_games),
                                reverse=True)[:n]:
            ast = ""
            if word in self.possible_games:
                if not self.mode == "strict" and not only_possible_answers:
                    ast = "*"
            elif only_possible_answers:
                continue
            print("{:5s}{:3s}{:.4f}".format(word, ast, entr))
        if not self.mode == "strict" and not only_possible_answers:
            print("* = Possible answer")

    def readLexicon(self, lexicon):
        with open(lexicon) as f:
            self.all_words = {word.strip(): word.strip() for word in f.readlines()}

    def loadCachedEntropies(self, cache_file):
        entropies = None
        if os.path.exists(cache_file):
            with open(cache_file) as f:
                data = defaultdict(type(None))
                data.update(json.load(f))
                if data["hash"] == self.lexicon_hash:
                    entropies = data["entropies"]
        return entropies

    def saveCachedEntropies(self, entropies, cache_file):
        with open(cache_file, 'w') as f:
            json.dump({"hash": self.lexicon_hash,
                       "entropies": entropies}, f)

    def entropy(self, word):
        pattern_freq = np.zeros([self.n_char_patterns]*len(word), dtype='i')
        for game in self.possible_games.values():
            pattern = game.guess(word)
            pattern_freq[tuple(pattern)] += 1
        pattern_freq = pattern_freq[pattern_freq.nonzero()]
        prob = pattern_freq / np.sum(pattern_freq)
        return -np.sum(prob * np.log2(prob))

    def calculateEntropies(self, guesses):
        entropies = {}
        for i,word in enumerate(guesses):
            line = "Calculating entropy: " \
                   "{:s} ({:d}/{:d})".format(word, i, len(guesses))
            print(line, end='\r')
            sys.stdout.flush()
            entropies[word] = self.entropy(word)
        print(re.sub(".", " ", line), end="\r")
        sys.stdout.flush()
        return entropies

    def validGuess(self, guess):
        match = re.match(r"[a-z]{5}$", guess.lower())
        if match is None:
            print("Not a five-letter word")
            return None
        return match.group()

    def validPattern(self, pattern):
        match = re.match(r"[0-2]{5}$", pattern)
        if match is None:
            print("Invalid result pattern")
            return None
        return match.group()

    def inputPattern(self):
        # Pattern input
        pattern = None
        for _ in range(20):
            pattern = input("Result ({}) (or [r]eturn): ".format(self.pattern_rules))
            if len(pattern) == 0:
                continue
            if pattern.lower() == "r":
                return None
            pattern = self.validPattern(pattern)
            if pattern is None:
                continue
            break
        if pattern is None:
            raise RuntimeError("Invalid result pattern.")
        return pattern

    @property
    def Game(self):
        return Wordle

    @property
    def n_char_patterns(self):
        return 3

    @property
    def pattern_rules(self):
        return "0=absent, 1=wrong position, 2=correct"
