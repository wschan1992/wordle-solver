# Perform most operations in the number representation of unicode, i.e. ord()
# For the sake of speed, avoid chr/ord conversion as much as possible.
# Unicode reference: https://en.wikipedia.org/wiki/Kana#In_Unicode

#-------------------------------------------------------------------------------
def ordHira2kata(ord_c):
    if (ord_c > 0x3040 and ord_c <= 0x3096) or \
       (ord_c > 0x309C and ord_c <= 0x309F):
        return ord_c + 0x60
    return ord_c

def hira2kata(s):
    if len(s) > 1:
        return "".join(hira2kata(c) for c in s)
    return chr(ordHira2kata(ord(s)))

#-------------------------------------------------------------------------------
def ordKata2hira(ord_c):
    if (ord_c > 0x30A0 and ord_c <= 0x30F6) or \
       (ord_c > 0x30FC and ord_c <= 0x30FF):
        return ord_c - 0x60
    return ord_c

def kata2hira(s):
    if len(s) > 1:
        return "".join(kata2hira(c) for c in s)
    return chr(ordKata2hira(ord(s)))

#-------------------------------------------------------------------------------
_ord_kogaki = [0x3041, 0x3043, 0x3045, 0x3047, 0x3049,  # ぁぃぅぇぉ
               0x3063, 0x3083, 0x3085, 0x3087]  # っゃゅょ
def ordKogaki2normal(ord_c):
    ord_c_hira = ordKata2hira(ord_c)
    if ord_c_hira in _ord_kogaki:
        return ord_c + 1
    if ord_c_hira == 0x3095:  # ヵ
        return ord_c - 0x4A
    if ord_c_hira == 0x3096:  # ヶ
        return ord_c - 0x45
    return ord_c

def kogaki2normal(s):
    if len(s) > 1:
        return "".join(kogaki2normal(c) for c in s)
    return chr(ordKogaki2normal(ord(s)))

#-------------------------------------------------------------------------------
_ord_kasatagyou = list(range(0x304C, 0x3063, 2)) \
                  + list(range(0x3065, 0x306A, 2))  # かさた行
_ord_hagyou = list(range(0x306F, 0x307E))  # は行
def ordDakuon2seion(ord_c):
    ord_c_hira = ordKata2hira(ord_c)
    if ord_c_hira in _ord_kasatagyou:
        return ord_c - 1
    if ord_c_hira in _ord_hagyou:
        return ord_c - ord_c % 3
    if ord_c_hira == 0x3094:  # う゛
        return ord_c - 0x4E
    return ord_c

def dakuon2seion(s):
    if len(s) > 1:
        return "".join(dakuon2seion(c) for c in s)
    return chr(ordDakuon2seion(ord(s)))

#-------------------------------------------------------------------------------
def ordBasicKana(ord_c):
    return ordKogaki2normal(ordDakuon2seion(ord_c))

def basicKana(s):
    if len(s) > 1:
        return "".join(_basicKana(c) for c in s)
    return chr(ordBasicKana(ord(s)))

def ordIsSameBasicKana(ord_c, ord_basic):
    ordBasicKana(ord_c) == ord_basic

#-------------------------------------------------------------------------------
def ordWhichColumn(ord_c):
    ord_c = ordKata2hira(ord_c)
    if ord_c < 0x3041 or ord_c > 0x309F:
        return None
    if ord_c < 0x306A:  # ぁあかがさざただ行
        if ord_c > 0x3063:  # skip っ
            ord_c -= 1
        return ord_c - (ord_c - 0x3041) % 10 + (ord_c - 0x3041) % 2
    if ord_c < 0x306F:  # な行
        return 0x306A
    if ord_c < 0x307E:  # はばぱ行
        return 0x306F + (ord_c - 0x306F) % 3
    if ord_c < 0x3083:  # ま行
        return 0x307E
    if ord_c < 0x3089:  # ゃや行
        return 0x3083 + (ord_c - 0x3083) % 2
    if ord_c < 0x308E:  # ら行
        return 0x3089
    if ord_c > 0x308E and ord_c < 0x3093:  # わ行
        return 0x308F
    return ord_c

def whichColumn(c):
    return chr(ordWhichColumn(ord(c)))

def ordWhichBasicColumn(ord_c):
    #return ordBasicKana(ordWhichColumn(ord_c)  # Too slow
    ord_c = ordKata2hira(ord_c)
    if ord_c < 0x3041 or ord_c > 0x309F:
        return None
    if ord_c < 0x304B:  # ぁあ行
        return 0x3042
    if ord_c < 0x306A:  # かがさざただ行
        if ord_c > 0x3063:  # skip っ
            ord_c -= 1
        return ord_c - (ord_c - 0x304B) % 10
    if ord_c < 0x306F:  # な行
        return 0x306A
    if ord_c < 0x307E:  # はばぱ行
        return 0x306F
    if ord_c < 0x3083:  # ま行
        return 0x307E
    if ord_c < 0x3089:  # ゃや行
        return 0x3083
    if ord_c < 0x308E:  # ら行
        return 0x3089
    if ord_c > 0x308E and ord_c < 0x3093:  # わ行
        return 0x308F
    return ord_c

def whichBasicColumn(c):
    return chr(ordWhichBasicColumn(ord(c)))

#-------------------------------------------------------------------------------
def ordWhichRow(ord_c):
    ord_c = ordKata2hira(ord_c)
    if ord_c < 0x3041 or ord_c > 0x309F:
        return None
    if ord_c < 0x306A:  # ぁあかがさざただ行
        if ord_c > 0x3063:  # skip っ
            ord_c -= 1
        return 0x3041 + (ord_c - 0x3041) % 10 + (ord_c - 0x3040) % 2
    if ord_c < 0x306F:  # な行
        return 0x3042 + (ord_c - 0x306A) * 2
    if ord_c < 0x307E:  # はばぱ行
        return 0x3042 + (ord_c - 0x306F) // 3 * 2
    if ord_c < 0x3083:  # ま行
        return 0x3042 + (ord_c - 0x307E) * 2
    if ord_c < 0x3089:  # ゃや行
        return 0x3042 + (ord_c - 0x3083) // 2 * 4
    if ord_c < 0x308E:  # ら行
        return 0x3042 + (ord_c - 0x3089) * 2
    if ord_c == 0x308E:  # ゎ
        return 0x3042
    if ord_c < 0x3093:  # わ行
        if ord_c > 0x3090:  # ゑを skipped う(wu)
            ord_c += 1
        return 0x3042 + (ord_c - 0x308F) * 2
    if ord_c == 0x3094:  # う゛(vu)
        return 0x3046
    return ord_c

def whichRow(c):
    return chr(ordWhichRow(ord(c)))
