from .Wordle import *
from .WordleSolver import *
import csv
from .jp_utils import *

class JPWordleSolver(WordleSolver):
    def __init__(self, mode="normal", max_guesses=0, max_suggests=25):
        super().__init__(mode, max_guesses, max_suggests)

    def readLexicon(self, lexicon):
        self.all_words = {}
        with open(lexicon) as tsv:
            read_tsv = csv.reader(tsv, delimiter="\t")
            for row in read_tsv:
                if row[0] not in self.all_words:
                    self.all_words[row[0]] = row[1]  # key=kana, value=kanji
                else:
                    self.all_words[row[0]] += '/' + row[1]

    def showEntropies(self, n=0, only_possible_answers=False, initial=False):
        entropies = self.starting_entropies if initial else self.entropies
        if n < 1:
            n = len(self.entropies)
        for word,entr in sorted(self.entropies.items(),
                                key=lambda x: (x[1], x[0] in self.possible_games),
                                reverse=True)[:n]:
            ast = ""
            if word in self.possible_games:
                if not self.mode == "strict" and not only_possible_answers:
                    ast = "*"
            elif only_possible_answers:
                continue
            print("{:s}{:3s}{:5s}  {:.4f}".format(word, ast,
                                                 self.all_words[word], entr))
        if not self.mode == "strict" and not only_possible_answers:
            print("* = Possible answer")

    def calculateEntropies(self, guesses):
        entropies = {}
        for i,word in enumerate(guesses):
            line = "Calculating entropy: " \
                   "{:s} ({:d}/{:d})".format(word, i, len(guesses))
            print(line, end='\r')
            sys.stdout.flush()
            entropies[word] = self.entropy(word)
        print(re.sub(r"[\u3040-\u30FF]", r"\u3000",
              re.sub(r"[^\u3040-\u30FF]", " ", line)), end="\r")
        sys.stdout.flush()
        return entropies

#-------------------------------------------------------------------------------
class KotonohaTangoSolver(JPWordleSolver):
    def validGuess(self, guess):
        match = re.match(r"[\u30A1-\u30F3\u30FC]{5}$", hira2kata(guess))
        if match is None:
            print("Not a five-katakana word")
            return None
        return match.group()

    def validPattern(self, pattern):
        match = re.match(r"[0-2]{5}$", pattern)
        if match is None:
            print("Invalid result pattern")
            return None
        return match.group()

#-------------------------------------------------------------------------------
class KotobadeAsobouSolver(JPWordleSolver):
    def __init__(self, mode="normal", max_guesses=0, max_suggests=25,
                 legacy=False, no_hint=False):
        super().__init__(mode, max_guesses, max_suggests)
        self.legacy = legacy
        self.no_hint = no_hint
        self.__n_char_patterns = 3 if self.no_hint else 6

    def readLexicon(self, lexicon):
        self.all_words = {}
        with open(lexicon) as tsv:
            read_tsv = csv.reader(tsv, delimiter="\t")
            for row in read_tsv:
                if self.legacy:
                    row[0] = kogaki2normal(row[0])
                if row[0] not in self.all_words:
                    self.all_words[row[0]] = row[1]  # key=kana, value=kanji
                else:
                    self.all_words[row[0]] += '/' + row[1]

    def loadCachedEntropies(self, cache_file):
        entropies = None
        if os.path.exists(cache_file):
            with open(cache_file) as f:
                data = defaultdict(type(None))
                data.update(json.load(f))
                if data["hash"] == self.lexicon_hash and \
                   data["legacy"] == self.legacy and \
                   data["hint"] == (not self.no_hint):
                    entropies = data["entropies"]
        return entropies

    def saveCachedEntropies(self, entropies, cache_file):
        with open(cache_file, 'w') as f:
            json.dump({"hash": self.lexicon_hash,
                       "legacy": self.legacy,
                       "hint": not self.no_hint,
                       "entropies": entropies}, f)

    def validGuess(self, guess):
        match = re.match(r"[\u3041-\u3094\u30FC]{4}$", kata2hira(guess))
        if match is None:
            print("Not a four-hiragana word")
            return None
        return match.group()

    def validPattern(self, pattern):
        match = re.match(r"[0-2\^\|\-]{4}$", pattern)
        if match is None:
            print("Invalid result pattern")
            return None
        return match.group()

    @property
    def Game(self):
        return Wordle if self.legacy or self.no_hint else KotobadeAsobou

    @property
    def n_char_patterns(self):
        return self.__n_char_patterns

    @property
    def pattern_rules(self):
        rules = "0=absent, 1=wrong position, 2=correct"
        if not self.no_hint:
            rules += ", '^'=variants, '-'=same row, '|'=same column"
        return rules
