
import regex as re
from collections import defaultdict
import numpy as np

class Wordle:
    def __init__(self, answer):
        self.answer = answer
        self.n_char = len(answer)
        self.freq = defaultdict(int)
        for c in self.answer:
            self.freq[c] += 1

    def guess(self, guess):
        not_guessed = dict(self.freq)
        # Start by assuming all wrong guesses (black, 0)
        result = [0] * self.n_char
        # Find all correct guesses (green, 2)
        for i in range(self.n_char):
            if guess[i] == self.answer[i]:
                result[i] = 2
                not_guessed[guess[i]] -= 1
        # Find wrong-position guesses (yellow, 1)
        for i in range(self.n_char):
            if result[i] == 2:
                continue
            if guess[i] in self.answer and not_guessed[guess[i]] > 0:
                result[i] = 1
                not_guessed[guess[i]] -= 1
        return result

    enum_pattern = {0:"0", 1:"1", 2:"2"}
    pattern_enum = {y:x for x,y in enum_pattern.items()}

    @classmethod
    def patternStringToList(self, pattern):
        return [self.pattern_enum[c] for c in pattern]

    @classmethod
    def patternListToString(self, pattern):
        return "".join(self.enum_pattern[i] for i in pattern)

    @classmethod
    def hardModeRestricted(self, guess, result, allowed_words):
        if isinstance(result, str):
            result = self.patternStringToList(result)
        req_char = {}  # Require greens positions:characters
        req_freq = defaultdict(int)  # Required yellows/greens frequencies
        for i in range(len(guess)):
            if result[i] == 1:
                req_freq[guess[i]] += 1
            elif result[i] == 2:
                req_freq[guess[i]] += 1
                req_char[i] = guess[i]
        restricted_list = list(allowed_words)
        for word in allowed_words:
            removed=False
            # Check for greens positions:characters
            for i in req_char:
                if not word[i] == req_char[i]:
                    restricted_list.remove(word)
                    removed=True
                    break
            if removed: continue
            # Check for yellows/greens frequencies
            freq = defaultdict(int)
            for c in word:
                freq[c] += 1
            for c in req_freq:
                if freq[c] < req_freq[c]:
                    restricted_list.remove(word)
                    break
        return restricted_list

#-------------------------------------------------------------------------------
from .jp_utils import *

class KotobadeAsobou(Wordle):
    def __init__(self, answer):
        super().__init__(answer)
        self.ord_basic_kana = [ordBasicKana(ord(c)) for c in answer]
        self.ord_which_row = [ordWhichRow(ord(c)) for c in answer]
        self.ord_which_column = [ordWhichBasicColumn(ord(c)) for c in answer]

    def guess(self, guess):
        not_guessed = dict(self.freq)
        # Start by assuming all wrong guesses (black, 0)
        result = [0] * self.n_char
        # Find all correct guesses (green, 2),
        # then all variants ("^", 3)
        for i in range(self.n_char):
            c = guess[i]
            if self.answer[i] == c:
                result[i] = 2
                not_guessed[c] -= 1
        # Now find wrong positions (yellow, 1),
        # then all variants ("^", 3),
        # then finally, hints for same row ("-", 4) or same column ("|", 5)
        for i in range(self.n_char):
            c = guess[i]
            ord_c = ord(c)
            if not result[i] == 0:
                continue
            elif ordBasicKana(ord_c) == self.ord_basic_kana[i]:
                result[i] = 3  # "^"
            elif c in self.answer and not_guessed[c] > 0:
                result[i] = 1
                not_guessed[c] -= 1
            elif ordWhichRow(ord_c) == self.ord_which_row[i]:
                result[i] = 4  # "-"
            elif ordWhichBasicColumn(ord_c) == self.ord_which_column[i]:
                result[i] = 5  # "|"
        return result

    enum_pattern = {0:"0", 1:"1", 2:"2",
                   3:"^", 4:"-", 5:"|"}
    pattern_enum = {y:x for x,y in enum_pattern.items()}
