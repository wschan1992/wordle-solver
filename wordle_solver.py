#!/usr/bin/env python3

import os, sys, argparse
from modules.WordleSolver import WordleSolver

#===============================================================================
if __name__ == "__main__":
    parser = argparse.ArgumentParser('wordle_solver.py')

    parser.add_argument("--mode", "-m", default="normal",
                        choices=['normal', 'hard', 'strict'],
                        help="Game mode ('normal', 'hard' or 'strict'). " \
                             "'strict' mode: " \
                             "Only allowed to guess possible answers according " \
                             "to all known information.")
    parser.add_argument("--max-suggests", "-n", type=int, default=10,
                        help="Maxinum number of suggestions (0 = no limit).")
    parser.add_argument("--max-guesses", "-g", type=int, default=6,
                        help="Maxinum number of guesses (0 = no limit).")
    parser.add_argument("--lexicon", "-l", type=str,
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                             "data", "wordle-answers-alphabetical.txt"),
                        help="Path to the list of possible words. " \
                             "One word per line.")

#===============================================================================
def wordle_solver(args):
    solver = WordleSolver(mode=args.mode, max_guesses=args.max_guesses,
                          max_suggests=args.max_suggests)
    solver.initialize(args.lexicon)
    solver.startNewGame()

#===============================================================================
if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])
    exit(wordle_solver(args))
