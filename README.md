## Wordle Solver

Simple scripts that run in command line terminals to provide suggestions for
solving the game [Wordle](https://www.nytimes.com/games/wordle/index.html)
(or its Japanese variants, [Kotobade Asobou](https://taximanli.github.io/kotobade-asobou/)
and [Kotonoha Tango](https://plum-chloride.jp/kotonoha-tango/index.html)).

The algorithm is a greedy one that maximises entropy for each guess,
without looking ahead. The algorithm is only locally optimal.
